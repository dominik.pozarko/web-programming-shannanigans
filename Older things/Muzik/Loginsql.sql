DROP DATABASE IF EXISTS MuzikProfile;
Create DATABASE MuzikProfile;
USE MuzikProfile;
DROP TABLE IF EXISTS Profile;
CREATE TABLE Profile (
ID int auto_increment primary key,
    Name char(30) not null,
    Surname char(30) not null,
    Nickname char(30) not null,
    Pass char(30) not null,
    Email char(50) not null,
    Birthdate date not null,
    Picture char(50),
    Description char(100),
    Creationdate date
)engine=MyISAM;
DROP TABLE IF EXISTS Post;
CREATE TABLE Post(
Post_id int auto_increment primary key,
    Owner_id int not null,
    Likes int,
    Comments int,
    Song char(50) not null,
    Song_name char(50) not null,
    Image char(50) not null,
    Description char(100),
    Post_creation_date datetime
)engine=MyISAM;
DROP TABLE IF EXISTS Comment;
CREATE TABLE Comment(
Comment_id int auto_increment primary key,
    Post_id int,
    Owner_id int,
    Likes int,
    Input char(100),
    Comment_creation_date date
)
engine=MyISAM;