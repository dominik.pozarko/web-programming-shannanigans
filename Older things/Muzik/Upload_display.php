<!doctype html>
<html>
<body>
    <link rel="stylesheet" href="style.css">
    <div id="Post">
        
        <div id="Post_Image">
        <img src="<?php echo $Slika[$i]; ?>" id="Post_Image_Image">
        </div>
        <div id="Important">
        <div id="Uploader_profile">
            <img src="<?php echo $Profilna; ?>" id="Uploader_slika">  
            <p id="Profilni_Nadimak"><?php echo $Nadimak; ?></p>
            </div>
            <br>
        <div id="Song">
            <p id="Ime_pijesme"><?php
                echo $Ime_pijesme[$i];?></p>
            <audio controls>
            <source src="<?php echo $Pijesma[$i];?>" type="audio/mpeg">
            </audio>
            </div>
        </div>
        <div id="Opis_uploada">
        <div id="Opis_pijesme">
            <p id="Opistxt"><?php echo $Opis[$i];?></p>
            </div>
            <div id="Rating">
                <p class="rates" id="Like">❤️ <?php echo $Likeovi[$i]; ?></p><p class="rates" id="Comment"> 💬 <?php echo $Komentari[$i];?></p>
                <p id="Upload_date"><?php echo $Objavljeno[$i]; ?></p>
            </div>
        </div>
    </div>
    </body>
</html>