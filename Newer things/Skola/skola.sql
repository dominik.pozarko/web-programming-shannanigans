DROP DATABASE IF EXISTS skola;
Create DATABASE skola;
USE skola;
DROP TABLE IF EXISTS predmeti;
CREATE TABLE predmeti (
ID_predmeta int auto_increment primary key,
    ime_predmeta char(30) not null,
    opis_predmeta text not null,
    broj_sati_u_tjednu int(20) not null
)
engine=MyISAM;
CREATE TABLE korisnik (
ID_korisnika int auto_increment primary key,
    Nick char(30) not null,
    Pass char(30) not null
)
engine=MyISAM;
CREATE TABLE skole (
ID_skole int auto_increment primary key,
    Naziv_skole char(30) not null,
    Mjesto char(30) not null,
    Zupanija char(30) not null,
    Ravnatelj char(50) not null,
    Broj_ucenika char(30) not null
)engine=MyISAM;