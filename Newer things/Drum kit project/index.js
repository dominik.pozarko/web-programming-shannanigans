for (var i = 0; i<document.querySelectorAll("button,.drum").length; i++){
    document.querySelectorAll("button,.drum")[i].addEventListener("click", function (){
        button=this.classList;
        for (var j=0; j<button.length; j++){
            if (button[j]!="drum" && button[j].length==1)
                button=button[j];
            break;
        }
        playSound(button);
        buttonAnimation(button);
    });
}
document.addEventListener("keydown", function(event){
    playSound(event.key);
    buttonAnimation(event.key);
});

function playSound(input){
    var crash = new Audio("sounds/crash.mp3");
    var kick = new Audio("sounds/kick-bass.mp3");
    var snare = new Audio("sounds/snare.mp3");
    var tom1 = new Audio("sounds/tom-1.mp3");
    var tom2 = new Audio("sounds/tom-2.mp3");
    var tom3 = new Audio("sounds/tom-3.mp3");
    var tom4 = new Audio("sounds/tom-4.mp3");
    switch(input){
        case "w":
            crash.play();
            break;
        case "a":
            kick.play();
            break;
        case "s":
            snare.play();
            break;
        case "d":
            tom1.play();
            break;
        case "j":
            tom2.play();
            break;
        case "k":
            tom3.play();
            break;
        case "l":
            tom4.play();
            break;
    }
}

function buttonAnimation(currentKey){
    var currentButton=document.querySelector("."+currentKey);
    currentButton.classList.add("pressed");
    setTimeout(function(){
        currentButton.classList.remove("pressed");
    }, 100);
}

// function HouseKeeper (name, yearsExperience, cleaningRepertoire) {
//     this.name = name;
//     this.yearsExperience = yearsExperience;
//     this.cleaningRepertoire = cleaningRepertoire;
//     this.clean = function() {
//         alert("I have cleaned my master UwU - " + this.name);
//     }
// }
// var houseKeepers=[];
// houseKeepers.push(new HouseKeeper("Anna",13,["jonboe","bonjoe"]));
// alert(houseKeepers[0].yearsExperience);
// houseKeepers[0].clean();