import express from "express";
const port = 3000;
let app = express();

app.get("/", (req, res) => {
    res.send("<h1>Hello world!</h1>");
});

app.get("/contact", (req, res) => {
    res.send("<h1>Contact</h1>");
});

app.get("/about", (req, res) => {
    res.send("<h1>About</h1>");
});

app.listen(port, () => {
    console.log("Server running on port "+port+".");
});