import express from "express";
const port = 3000;
let app = express();

app.listen(port, () => {
    console.log('Server running on port ' + port + '.');
});