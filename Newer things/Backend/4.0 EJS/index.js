import express from "express";
import { dirname } from "path";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));
const app = express();
const port = 3000;
let currentDate = new Date();

let isWeekend = false;

if (currentDate.getDay()===0 || currentDate.getDay()===6)
    isWeekend= true;
else
    isWeekend = false;

app.get("/", (req, res) => {
    res.render("index.ejs", { isWeekend});
    console.log(currentDate.getDay());
    console.log(isWeekend);

});

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
  });