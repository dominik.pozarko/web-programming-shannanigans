import express from "express";
import bodyParser from "body-parser";

const app = express();
const port = 3000;
let letterLength=0;

app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.render("./index.ejs");
});

app.post("/submit", (req, res) => {
  letterLength+=req.body["fName"].length;
  letterLength+=req.body["lName"].length;
  res.render("index.ejs", {
    nameLength: letterLength,
  } );
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
