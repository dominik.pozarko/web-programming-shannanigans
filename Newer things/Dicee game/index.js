
function switchImg (img){
    var image="";
    if (img==1)
        image=".img1";
    else
        image=".img2";
    var randomNumber=Math.random();
    randomNumber*=6;
    randomNumber=Math.ceil(randomNumber);
    switch(randomNumber){
        case 1:
            document.querySelector(image).src="images/dice1.png";
            break;
        case 2:
            document.querySelector(image).src="images/dice2.png";
            break;
        case 3:
            document.querySelector(image).src="images/dice3.png";
            break;
        case 4:
            document.querySelector(image).src="images/dice4.png";
            break;
        case 5:
            document.querySelector(image).src="images/dice5.png";
            break;
        case 6:
            document.querySelector(image).src="images/dice6.png";
            break;
    }
    return randomNumber;
}

var randomNumber1=switchImg(1);
var randomNumber2=switchImg(2);
if (randomNumber1>randomNumber2)
    document.querySelector("h1").innerHTML="PLAYER 1 WINS";
else if (randomNumber2>randomNumber1)
    document.querySelector("h1").innerHTML="PLAYER 2 WINS";
else
    document.querySelector("h1").innerHTML="DRAW";