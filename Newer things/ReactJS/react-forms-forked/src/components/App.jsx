import React, { useState } from "react";

function App() {
  const [name, changeName] = useState("");
  const [newName, setNewName] = useState("");

  function handleChange(event) {
    console.log(event.target.value);
    changeName(event.target.value);
  }

  function handleSubmit() {
    setNewName(name);
  }

  return (
    <div className="container">
      <h1>Hello {newName}</h1>
      <input
        onChange={handleChange}
        type="text"
        placeholder="What's your name?"
        value={name}
      />
      <button onClick={handleSubmit}>Submit</button>
    </div>
  );
}

export default App;
