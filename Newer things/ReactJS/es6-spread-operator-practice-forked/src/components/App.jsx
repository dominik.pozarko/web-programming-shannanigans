import React, { useState } from "react";

function App() {
  const [itemText, setItemText] = useState("");
  const [items, setItems] = useState([]);

  function handleChange(event) {
    const value = event.target.value;
    setItemText(value);
  }

  function submitItems() {
    const newItem = itemText;
    setItems((prevValue) => {
      return [...prevValue, newItem];
    });
  }

  return (
    <div className="container">
      <div className="heading">
        <h1>To-Do List</h1>
      </div>
      <div className="form">
        <input onChange={handleChange} type="text" value={itemText} />
        <button onClick={submitItems}>
          <span>Add</span>
        </button>
      </div>
      <div>
        <ul>
          {items.map((item) => (
            <li>{item}</li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default App;
