import React from "react";
import Login from "./loginComponent";
import Input from "./input";

var isLoggedIn = true;

function App() {
  return (
    <div className="container">{isLoggedIn ? <h1>Hello!</h1> : <Login />}</div>
  );
}

export default App;
