import React, { useState } from "react";

function App() {
  const [contact, setContact] = useState({
    fName: "",
    lName: "",
    email: "",
  });
  let newValues = { fName: "", lName: "", email: "" };

  function updateValues(event) {
    const { name, value } = event.target;

    if (name === "fName") {
      newValues = {
        fName: value,
        lName: newValues.lName,
        email: newValues.email,
      };
    } else if (name === "lName") {
      newValues = {
        fName: newValues.fName,
        lName: value,
        email: newValues.email,
      };
    } else if (name === "email") {
      newValues = {
        fName: newValues.fName,
        lName: newValues.lName,
        email: value,
      };
    }
  }

  function updateForm(event) {
    setContact(newValues);
    event.preventDefault();
  }

  return (
    <div className="container">
      <h1>
        Hello {contact.fName} {contact.lName}
      </h1>
      <p>{contact.email}</p>
      <form onSubmit={updateForm}>
        <input onChange={updateValues} name="fName" placeholder="First Name" />
        <input onChange={updateValues} name="lName" placeholder="Last Name" />
        <input onChange={updateValues} name="email" placeholder="Email" />
        <button>Submit</button>
      </form>
    </div>
  );
}

export default App;
