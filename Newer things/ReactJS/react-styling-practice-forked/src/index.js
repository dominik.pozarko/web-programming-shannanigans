//Create a React app from scratch.
//Show a single h1 that says "Good morning" if between midnight and 12PM.
//or "Good Afternoon" if between 12PM and 6PM.
//or "Good evening" if between 6PM and midnight.
//Apply the "heading" style in the styles.css
//Dynamically change the color of the h1 using inline css styles.
//Morning = red, Afternoon = green, Night = blue.
import React from "react";
import ReactDOM from "react-dom";
let messageStyle = {
  color: "",
};
let timeHour = new Date().getHours();
let returnMessage = "";
if (timeHour < 12) {
  returnMessage = "Good morning";
  messageStyle.color = "red";
} else if (timeHour < 18) {
  returnMessage = "Good afternoon";
  messageStyle.color = "green";
} else {
  returnMessage = "Good evening";
  messageStyle.color = "blue";
}
ReactDOM.render(
  <div>
    <h1 className="heading" style={messageStyle}>
      {returnMessage} {timeHour}
    </h1>
  </div>,
  document.getElementById("root")
);
