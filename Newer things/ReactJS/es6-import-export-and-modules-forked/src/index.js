import React from "react";
import ReactDOM from "react-dom";
// import * as pi from "./math";
import pi, { doublePi, triplePi } from "./math";
//pi.default
ReactDOM.render(
  <ul>
    <li>{pi}</li>
    <li>{doublePi()}</li>
    <li>{triplePi()}</li>
  </ul>,
  document.getElementById("root")
);
