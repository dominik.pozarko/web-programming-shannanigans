import React, { useState } from "react";

function App() {
  const [color, changeColor] = useState("white");

  function blackOut() {
    changeColor("black");
  }

  function whiteOut() {
    changeColor("white");
  }

  return (
    <div className="container">
      <h1>Hello</h1>
      <input type="text" placeholder="What's your name?" />
      <button
        onMouseOver={blackOut}
        onMouseOut={whiteOut}
        style={{ backgroundColor: color }}
      >
        Submit
      </button>
    </div>
  );
}

export default App;
