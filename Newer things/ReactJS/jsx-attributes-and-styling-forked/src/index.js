import React from "react";
import ReactDOM from "react-dom";

const Mlinci =
  "https://img.sndimg.com/food/image/upload/q_92,fl_progressive,w_1200,c_scale/v1/img/recipes/39/67/12/pic7OoRbt.jpg";
const Palacinke =
  "https://imageproxy.wolt.com/menu/menu-images/5f6a0c0fa1b06e69bfb45a03/b5bc617e-2036-11ec-a74c-0ead9b0f60f8_palacinkehr_product_4.jpeg";
const Lasagne =
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlIA77LoN5u5AcuiHoHdoAgQN28gYjSZ3F1g&s";

ReactDOM.render(
  <div>
    <h1 className="heading">My Favourite Foods</h1>
    <div>
      <img className="foodImage" alt="Mlinci" src={Mlinci} />
      <img className="foodImage" alt="Palačinke" src={Palacinke} />
      <img className="foodImage" alt="Lasane" src={Lasagne} />
    </div>
  </div>,
  document.getElementById("root")
);
