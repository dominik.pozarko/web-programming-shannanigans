import React, { useState } from "react";

function CreateArea(props) {
  const [inputText, updateInput] = useState({ title: "", content: "" });

  function handleChange(event) {
    const { name, value } = event.target;
    updateInput((prevNote) => {
      return { ...prevNote, [name]: value };
    });
  }

  return (
    <div>
      <form
        onSubmit={(event) => {
          event.preventDefault();
        }}
      >
        <input
          name="title"
          onChange={handleChange}
          placeholder="Title"
          value={inputText.title}
        />
        <textarea
          name="content"
          onChange={handleChange}
          value={inputText.content}
          placeholder="Take a note..."
          rows="3"
        />
        <button
          onClick={() => {
            props.addNote(inputText);
            updateInput({ title: "", content: "" });
          }}
        >
          Add
        </button>
      </form>
    </div>
  );
}

export default CreateArea;
