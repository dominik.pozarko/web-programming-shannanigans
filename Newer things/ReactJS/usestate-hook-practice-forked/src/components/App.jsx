import React, { useState } from "react";

function App() {
  const [state, setState] = useState("TIME");

  function updateTime() {
    let time = new Date().toLocaleTimeString();
    setState(time);
  }
  setInterval(updateTime, 1000);

  function updateTime() {
    let time = new Date().toLocaleTimeString();
    // time = time.replace(/\s/g, "");
    setState(time);
  }

  return (
    <div className="container">
      <h1>{state}</h1>
      <button onClick={updateTime}>Get Time</button>
    </div>
  );
}

export default App;
