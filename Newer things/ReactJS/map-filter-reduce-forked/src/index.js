// var numbers = [3, 56, 2, 48, 5];

// //Map -Create a new array by doing something with each item in an array.

// function double(x) {
//   return x * 2;
// }
// const doubles = numbers.map(double);
// console.log(doubles);
// //Filter - Create a new array by keeping the items that return true.

// const newFunction = numbers.filter(function (x) {
//   return x > 10;
// });
// console.log(newFunction);
// //Reduce - Accumulate a value by doing something to each item in an array.

// numbers.reduce(function (accumulator, currentNumber) {
//   console.log("accumulator = " + accumulator);
//   console.log("currentNumber = " + currentNumber);
//   return accumulator + currentNumber;
// });

// //Find - find the first item that matches from an array.

// const newNumber = numbers.find(function (num) {
//   return num > 10;
// });
// console.log(newNumber);

// //FindIndex - find the index of the first item that matches.

// const newIndex = numbers.findIndex(function (num) {
//   return num > 10;
// });
// console.log(newIndex);

import emojipedia from "./emojipedia";

function returnTexts(text) {
  return text.meaning.substring(0, 100);
}

const newMeaning = emojipedia.map(returnTexts);
console.log(newMeaning);
