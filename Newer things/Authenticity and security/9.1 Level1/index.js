import express from "express";
import bodyParser from "body-parser";
import pg from "pg";

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

const db = new pg.Client({
  user: "postgres",
  host: "localhost",
  database: "users",
  password: "",
  port: 5432
});

db.connect();

app.get("/", (req, res) => {
  res.render("home.ejs");
});

app.get("/login", (req, res) => {
  res.render("login.ejs");
});

app.get("/register", (req, res) => {
  res.render("register.ejs");
});

app.post("/register", async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  await db.query("INSERT INTO users (email,password) VALUES ($1,$2)", [username,password]);
  console.log(username);
  console.log(password);
  res.redirect("/");
});

app.post("/login", async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const results = await db.query("SELECT * FROM users WHERE email = $1 and password = $2", [username,password]);
  console.log(username);
  console.log(password);
  if (results.rows.length>0){
    res.render("secrets.ejs");
  }
  else {
    res.redirect("/");
  }
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
